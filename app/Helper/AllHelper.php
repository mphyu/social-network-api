<?php
namespace App\Helper;

class AllHelper{
    function successMessage($message){
        return response()->json([
            'result' => 1,
            'message' => $message
        ]);
    }
    function responseData($data) {
        return response()->json([
            'result' => 1,
            'data' => $data
        ]);
    }
    function success($token,$data){
        return response()->json([
            'result' => 1,
            'token' => $token,
            'data' => $data
        ]);
    }

    function failMessage($message) {
        return response()->json([
            'result' => 0,
            'message' => $message
        ]);
    }
}

?>
