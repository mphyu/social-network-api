<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Requests\PagePostCreateRequest;
use App\Http\Requests\PostCreateRequest;
use App\Models\Page;
use App\Models\Post;
use App\Helper\AllHelper;
use Auth;

class PostController extends Controller
{
    //Page Attach Post
    public function pageAttachPost($pageId, PagePostCreateRequest $request) {
        //Check whether page id is include or not.
        if(empty($pageId)) return AllHelper::failMessage('Page Id is required');

        //Check whether page id is exist or not.
        $page = Page::find($pageId);
        if(!$page) return AllHelper::failMessage('Page Id is not found. Please check again');

        return $this->createPost($request->post_content,$page_id);
    }

     //Person Attach Post
     public function personAttachPost(PostCreateRequest $request) {
        return $this->createPost($request->post_content);
    }

    //Create Post
    private function createPost($post_content,$page_id = NULL) {
        $column_name = '';
        $id = Auth::id();
        if(!empty($page_id)){
            $column_name = 'page_id';
            $id = $page_id;
        }else{
            $column_name = 'user_id';
        }

        Post::create([
            'post_content' => $post_content,
            $column_name => $id,
        ]);
        return AllHelper::successMessage('Post Successfully Created.');
    }
    //Show Feed Of The Currently Logged In User
    //Ordered By Id Desc
    public function feed() {
        $result = Post::where(function($q) {
                        $q->has("user.follower")->orHas("page.follower");
                    })
                    ->with('user:id,first_name,last_name','page:id,page_name')
                    ->orderBy('id', 'desc')
                    ->get();

        return AllHelper::responseData($result);
    }
}
