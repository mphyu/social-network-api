<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Helper\AllHelper;
use Auth;

class AuthController extends Controller
{
    //User Register
    public function register(RegisterRequest $request) {
        //Store User Data To Table
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email' => $request->email,
            'password' =>  Hash::make($request->password),
        ]);

        return AllHelper::responseData($user);
    }

    //User Login
    public function login(LoginRequest $request) {
        //Check email and password is correct.
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $token = $user->createToken('social');

            return AllHelper::success($token->plainTextToken,$user);
        }

        return AllHelper::failMessage('Email And Password Doesn\'t Match.');
    }

}
