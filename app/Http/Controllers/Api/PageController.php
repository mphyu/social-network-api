<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageCreateRequest;
use App\Models\Page;
use App\Helper\AllHelper;
use Auth;

class PageController extends Controller
{
    //Create Page
    public function create(PageCreateRequest $request) {
        //Store Page Data To Table
        $result = Page::where('user_id', Auth::id())->first();
        if($result) return AllHelper::failMessage('Sorry!You have reached page limit.');
        $page = Page::create([
            'page_name' => $request->page_name,
            'user_id' => Auth::id(),
        ]);

        return AllHelper::successMessage('Page Successfully Created.');
    }

}
