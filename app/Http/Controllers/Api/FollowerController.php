<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Follower;
use App\Helper\AllHelper;
use Auth;

class FollowerController extends Controller
{
     //Follow Person
     public function followPerson($personId) {
        return $this->following($personId, 'person_following_id');
    }

    public function followPage($pageId) {
        return $this->following($pageId,'page_following_id');
    }

    private function following($id,$following_id) {

         //Check whether page id is include or not.
         if(empty($id)) return AllHelper::failMessage('Id is required');

         //Check whether page or user id is exist or not.
         $existingId= (($following_id == 'person_following_id')?'App\Models\User':'App\Models\Page') ::find($id);
         if(empty($existingId)) return AllHelper::failMessage('ID doesn\'t exist. Please check again');

         //Check whether already following or not.
         //If already following, unfollowing the given id.
         $login_user_id = Auth::id();
         $follow = Follower::where('follower_id',$login_user_id)->where($following_id, $id)->first();

         if(isset($follow)) {
             $follow->delete();
             return AllHelper::failMessage('UnFollowing Successfully.');
         }

         //Allow to follow only one person and page
         $following = $this->checkFollowLimit($following_id);
         if($following) return AllHelper::failMessage('Sorry! You have reached following limit.');

        Follower::create([
            'follower_id' =>  Auth::id(),
             $following_id => $id
        ]);
        return AllHelper::successMessage('Following Successfully.');
    }

    private function checkFollowLimit($following_id) {
        $result = Follower::where('follower_id',Auth::id())->where($following_id,'<>', NULL)->first();
        return $result;
    }

}
